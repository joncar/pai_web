<?php if($this->user->log): ?>
<div id="sidebar" class="sidebar responsive">
        <ul class="nav nav-list">
            <li>
                <a href="<?= site_url('panel') ?>">
                        <i class="menu-icon fa fa-tachometer-alt"></i>
                        <span class="menu-text">Escritorio</span>
                </a>
                <b class="arrow"></b>
            </li>
             <!--- Alumnos --->
             <?php 
                    $menu = array(
                        'vacunas'=>array('vacunacion'),                                            
                        'movimientos'=>array('registrar_vacunas'),
                        'maestras'=>array('region','distrito','dosis','edades','local_servicio','nivel_escolar','procesos_vacunas','sexo','zona','zona_vacunacion','tipo_contador'),
                        'reportes'=>array(
                            'rep/newreportes','rep/report_organizer','rep/mis_reportes',                           
                        ),
                        'seguridad'=>array('ajustes','grupos','funciones','user','acciones')
                    );
                    $menu = $this->user->filtrarMenu($menu);
                    $label = array(
                        'movimientos'=>array('Movimientos','fa fa-medkit'),
                        'vacunas'=>array('Vacunas','fa fa-syringe'),
                        'vacunacion'=>array('vacunas'),
                         'reportes'=>array('Reportes','fa fa-file'),
                        'maestras'=>array('Archivo','fa fa-table'),
                        'seguridad'=>array('Seguridad','fa fa-user-secret')
                    );
             ?>
             <?php  echo getMenu($menu,$label); ?>            
        </ul>
       <div id="sidebar-collapse" class="sidebar-toggle sidebar-collapse">
            <i data-icon2="ace-icon fa fa-angle-double-right" data-icon1="ace-icon fa fa-angle-double-left" class="ace-icon fa fa-angle-double-left"></i>
        </div>
        <div style="color:white; background:#222222; font-size:8px; text-align:center">
            <a href="#" style="color:white;">
                <?= img('img/eva-01.svg','width:50%') ?>
            </a>
        </div>

        <script type="text/javascript">
                try{ace.settings.check('sidebar' , 'collapsed')
                ace.settings.sidebar_collapsed(true, true);
                }catch(e){}
        </script>
</div>
<?php endif ?>
