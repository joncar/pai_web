<div class="home-footer">
    <div class="page-wrapper">
        <div class="row">
            <div class="hidden-xs col-xs-12 col-sm-6 col-md-3 col-lg-3">
                <ul class="footer-nav pb20">
                    <li class="widget-container recent_properties_sidebar">
                        <h3 class="osLight footer-header">Lo más visitado en Food</h3>
                        <div class="propsWidget">
                            <ul class="propList">
                                <li>
                                    <a href="#">
                                        <div class="image">
                                            <?= img('img/logo.png','width:100%'); ?>
                                        </div>
                                        <div class="info text-nowrap">
                                            <div class="name">Producto 1</div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="hidden-xs col-sm-6 col-md-3 col-lg-3">
                <ul class="footer-nav pb20">
                    <li class="widget-container recent_properties_sidebar">
                        <h3 class="osLight footer-header">Lo más visitado en Beverages</h3>
                        <div class="propsWidget">
                            <ul class="propList">
                                <li>
                                    <a href="#">
                                        <div class="image">
                                            <?= img('img/logo.png','width:100%'); ?>
                                        </div>
                                        <div class="info text-nowrap">
                                            <div class="name">Producto 1</div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3" style="text-align:center">
                <ul class="footer-nav pb20">
                    <li class="widget-container widget_recent_entries"> 
                    <a href="http://ensissciences.com/webnew/files/CERTIFICADO%20ENSIS%20SCIENCES%20ISO%2022000%20SAPNISH.pdf">                       
                        <img src="http://ensissciences.com/webnew/img/iso.jpg" style="">
                    </li>
                         
                            <br>
                        <li class="widget-container social_sidebar">
                            <h3 class="osLight footer-header">Síguenos</h3>
                            <ul>

                                <li>
                                    <a href="https://www.facebook.com/pages/Ensis-Sciences/110958968924063" class="btn btn-sm btn-icon btn-round btn-o btn-white" target="_blank">
                                        <span class="fa fa-facebook"></span>
                                    </a>
                                    <a href="https://twitter.com/rsanzj" class="btn btn-sm btn-icon btn-round btn-o btn-white" target="_blank">
                                        <span class="fa fa-twitter"></span>
                            
                                    </a> 
                                    <a href="https://www.linkedin.com/in/ramses-sanz-58642130" class="btn btn-sm btn-icon btn-round btn-o btn-white" target="_blank">
                                        <span class="fa fa-linkedin"></span>
                                    </a> 
                                </li>
                            </ul>
                        </li>
                </ul>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3" style="text-align:center">
                <ul class="footer-nav pb20">
                    <li class="widget-container contact_sidebar">
                        <h3 class="osLight footer-header">Oficinas</h3>
                        <ul>
                            <li class="widget-phone"><span class="fa fa-phone"></span> <?= $this->ajustes->telefono ?></li>                            
                            <li class="widget-address osLight"><p style="line-height:16px;"><?= $this->ajustes->direccion_contacto ?></li>
                            <img src="http://ensissciences.com/webnew/img/Logo_EsadeCreapolis_CMYK1.jpg" style="">
                        </ul>

                </ul>
            </div>
        </div>
        <div class="copyright">© 2016 Ensis Sciences. Spain. All rights reserved</div>
    </div>
</div>