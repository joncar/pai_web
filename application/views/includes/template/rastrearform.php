<section style="text-align: right; padding: 0px 100px;">
    <form id="rastrear" class="form-inline" onsubmit="return rastrear()">
        <div class="form-group">
          <label for="exampleInputName2">Rastrea tu servicio, con Mensajeros ASAP siempre tendrás el control!</label>
          <input type="text" class="form-control" id="exampleInputName2" name="id" placeholder="#Rastreo">
        </div>
        <button type="submit" class="btn btn-default">Rastrear</button>
    </form>
</section>
<script>
    function rastrear(){
        var data = document.getElementById('rastrear');
        var datos = new FormData(data);        
        $.ajax({
            url:'<?= base_url('pedidos/frontend/consultarRastreo') ?>',
            data:datos,
            type:'post',
            processData:false,
            cache:false,
            contentType: false,
            success:function(data){
                data = JSON.parse(data);
                if(data.length>0){
                    document.location.href="<?= base_url('pedidos/frontend/rastrear') ?>/"+data[0].id;
                }else{
                    emergente('Lo sentimos pero no hemos podido encontrar su paquete, por favor verfique la información suministrada')
                }
            }
        });                    
        return false;
    }
</script>