<?= $output ?>
<script>
    $(document).on('ready',function(){
       $("#field-producto").focus();
       $("#field-producto").before('<div id="labelproduct"></div>');
       $(document).on('change','#field-producto',function(){
           $.post('<?= base_url('json/getProduct') ?>',{codigo:$(this).val()},function(data){
               data = JSON.parse(data);
               $("#labelproduct").html('<b>Producto comercial: </b>'+data.producto.nombre_comercial);
           });
       });
       
       $(document).on('keypress',"#field-producto",function(e){
          if(e.which==13){
            $("#field-producto").trigger('change');
            return false;
          }
       });
       
       $("#crudForm").on('success',function(){           
           $("#field-producto").focus();
       });
    });
</script>