<section class="clear:both" id="home-section-1" style="">
    <div class="container"><!-- container via hooks -->	
        <div id="page-content-container">	
            <div class="row-fluid">
                <div class="col-xs-12 col-sm-4 col-sm-offset-1">
                    <div class="form-container">
                        <?= $this->load->view('predesign/login') ?>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-sm-offset-1">
                    <div class="form-container">
                        <div>
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                          <li role="presentation" class="active">
                              <a href="#persona" aria-controls="persona" role="tab" data-toggle="tab">Soy una persona natural</a>
                          </li>
                          <li role="presentation">
                              <a href="#empresa" aria-controls="empresa" role="tab" data-toggle="tab">Soy una empresa</a>
                          </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="persona">
                                <div class="well">
                                    <form onsubmit="return sendData(this)">
                                        <div id="nombre_field_box" class="form-container">
                                            <label class="login-register-label" id="nombre_display_as_box" for="field-nombre">
                                                    Nombre<span class="required">*</span>  :
                                            </label>
                                            <div class="controls">
                                                <input type="text" maxlength="255" value="" class="form-control nombre" name="nombre" id="field-nombre">
                                            </div>
                                        </div>

                                        <div id="nombre_field_box" class="form-container">
                                            <label class="login-register-label" id="nombre_display_as_box" for="field-nombre">
                                                    Ciudad<span class="required">*</span>  :
                                            </label>
                                            <div class="controls">
                                                <?= form_dropdown_from_query('ciudades_id','ciudades','id','ciudades_nombre'); ?>
                                            </div>
                                        </div>

                                        <div id="email_field_box" class="form-container">
                                            <label class="login-register-label" id="email_display_as_box" for="field-email">
                                                    Email de contacto<span class="required">*</span>  :
                                            </label>
                                            <div class="controls">
                                                <input type="email" maxlength="255" value="" class="form-control email" name="email" id="field-email">
                                            </div>
                                        </div>

                                        <div id="celular_field_box" class="form-container">
                                                <label class="login-register-label" id="celular_display_as_box" for="field-celular">
                                                        Celular :
                                                </label>
                                                <div class="controls">
                                                    <input type="text" maxlength="255" value="" class="form-control celular" name="celular" id="field-celular">
                                                </div>
                                        </div>

                                        <div id="password_field_box" class="form-container">
                                                <label class="login-register-label" id="password_display_as_box" for="field-password">
                                                        Contraseña nuevo usuario<span class="required">*</span>  :
                                                </label>
                                                <div class="controls">
                                                    <input type="password" maxlength="255" value="" name="password" id="field-password" class="form-control">
                                                </div>
                                        </div>

                                        <div id="direccion_field_box" class="form-container">
                                                <label class="login-register-label" id="direccion_display_as_box" for="field-direccion">
                                                        Direccion :
                                                </label>
                                                <div class="controls">
                                                    <input type="text" maxlength="255" value="" class="form-control direccion" name="direccion" id="field-direccion">
                                                </div>
                                        </div>

                                        <div class="btn-group">
                                            <input type="hidden" name="tipo_registro" value="Persona">
                                            <button class="btn btn-success" type="submit" id="form-button-save">Registrarse</button>
                                        </div>
                                        <div class="alert" style="display:none"></div>
                                    </form>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="empresa">
                                <div class="well">
                                    <form onsubmit="return sendData(this)">
                                        <div id="nombre_field_box" class="form-container">
                                            <label class="login-register-label" id="nombre_display_as_box" for="field-nombre">
                                                    Razón Social<span class="required">*</span>  :
                                            </label>
                                            <div class="controls">
                                                <input type="text" maxlength="255" value="" class="form-control nombre" name="nombre" id="field-nombre">
                                            </div>
                                        </div>
                                        
                                        <div id="nombre_field_box" class="form-container">
                                            <label class="login-register-label" id="nombre_display_as_box" for="field-nombre">
                                                    RUC<span class="required">*</span>  :
                                            </label>
                                            <div class="controls">
                                                <input type="text" maxlength="255" value="" class="form-control ruc" name="ruc" id="field-ruc">
                                            </div>
                                        </div>

                                        <div id="nombre_field_box" class="form-container">
                                            <label class="login-register-label" id="nombre_display_as_box" for="field-nombre">
                                                    Ciudad<span class="required">*</span>  :
                                            </label>
                                            <div class="controls">
                                                <?= form_dropdown_from_query('ciudades_id','ciudades','id','ciudades_nombre'); ?>
                                            </div>
                                        </div>

                                        <div id="email_field_box" class="form-container">
                                            <label class="login-register-label" id="email_display_as_box" for="field-email">
                                                    Email de contacto<span class="required">*</span>  :
                                            </label>
                                            <div class="controls">
                                                <input type="email" maxlength="255" value="" class="form-control email" name="email" id="field-email">
                                            </div>
                                        </div>

                                        <div id="celular_field_box" class="form-container">
                                                <label class="login-register-label" id="celular_display_as_box" for="field-celular">
                                                        Celular :
                                                </label>
                                                <div class="controls">
                                                    <input type="text" maxlength="255" value="" class="form-control celular" name="celular" id="field-celular">
                                                </div>
                                        </div>
                                        
                                        <div id="celular_field_box" class="form-container">
                                                <label class="login-register-label" id="celular_display_as_box" for="field-celular">
                                                        Telefono :
                                                </label>
                                                <div class="controls">
                                                    <input type="text" maxlength="255" value="" class="form-control celular" name="telefono" id="field-telefono">
                                                </div>
                                        </div>

                                        <div id="password_field_box" class="form-container">
                                               <label class="login-register-label" id="password_display_as_box" for="field-password">
                                                        Contraseña nuevo usuario<span class="required">*</span>  :
                                                </label>
                                                <div class="controls">
                                                    <input type="password" maxlength="255" value="" name="password" id="field-password" class="form-control">
                                                </div>
                                        </div>

                                        <div id="direccion_field_box" class="form-container">
                                                <label class="login-register-label" id="direccion_display_as_box" for="field-direccion">
                                                        Direccion :
                                                </label>
                                                <div class="controls">
                                                    <input type="text" maxlength="255" value="" class="form-control direccion" name="direccion" id="field-direccion">
                                                </div>
                                        </div>

                                        <div class="btn-group">
                                            <input type="hidden" name="tipo_registro" value="Empresa">
                                            <button class="btn btn-success" type="submit" id="form-button-save">Registrarse</button>
                                        </div>
                                        <div class="alert" style="display:none"></div>
                                    </form>
                                </div>
                            </div>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>    
//Guardar    
function sendData(form){
    var datos = new FormData(form);
    var redirect = "<?= empty($_GET['redirect'])?base_url('panel'):$_GET['redirect'] ?>";
    $(".alert").hide();
    $.ajax({
        url:'<?= base_url('registro/index/insert_validation') ?>',
        data:datos,
        type:'post',
        processData:false,
        cache:false,
        contentType: false,
        success:function(data){
            data = data.replace('<textarea>','');
            data = data.replace('</textarea>','');
            data = JSON.parse(data);
            if(data.success){
                $.ajax({
                    url:'<?= base_url('registro/index/insert') ?>',
                    data:datos,
                    type:'post',
                    processData:false,
                    cache:false,
                    contentType: false,
                    success:function(data){
                        data = data.replace('<textarea>','');
                        data = data.replace('</textarea>','');
                        data = JSON.parse(data);
                        if(data.success){                                
                            $(".alert").addClass('alert-success').removeClass('alert-danger');
                            $(".alert").html(data.success_message);
                            $(".alert").show();
                            document.location.href=redirect;
                        }
                    }
                });
            }else{
                $(".alert").addClass('alert-danger').removeClass('alert-success');
                $(".alert").html(data.error_message);
                $(".alert").show();
            }
        }
    });
    return false;
}
</script>