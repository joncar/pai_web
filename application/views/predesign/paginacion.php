<?php 
    /* Requireds 
     * actual page $actual
     * total rows $total
     * count of button pages $pages
     * url $url
     * count of rows $rows
     */
?>
<?php $next = $actual+1; $previous = $actual-1==0?1:$actual-1; ?>
<?php $limit = $total/$rows; $limit = explode('.',$limit); ?>
<?php $residuo = !empty($limit[1])?$limit[1]:0; ?>
<?php $limit = $limit[0]; ?>
<?php $limit+= $residuo!=0?1:0; ?>
<?php $inicio = $actual/$pages ?>
<?php $inicio = explode('.',$inicio); ?>
<?php $inicio = $inicio[0]; $inicio = $inicio==0?1:$inicio; ?>
<ul class="pagination">
        <li>
            <a href="<?= str_replace('{{page}}',$previous,$url) ?>" aria-label="Previous">
                <span aria-hidden="true">&laquo;</span>
            </a>
        </li>
            <?php for($i=$inicio;$i<=$limit;$i++): ?>
                <li class='<?= $i==$actual?'active':'' ?>'><a href="<?= str_replace('{{page}}',$i,$url) ?>"><?= $i ?></a></li>
            <?php endfor ?>
        <li>
          <a href="<?= str_replace('{{page}}',$next,$url) ?>" aria-label="Next">
                <span aria-hidden="true">&raquo;</span>
          </a>
        </li>
  </ul>