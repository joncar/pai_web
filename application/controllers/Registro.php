<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('Main.php');
class Registro extends Main {
        const IDROLUSER = 3;
        public function __construct()
        {
            parent::__construct();            
        }

        public function index($url = 'main',$page = 0)
        {
            if($this->router->fetch_class()=='registro'){
                $crud = new ajax_grocery_CRUD();
                $crud->set_theme('bootstrap2');
                $crud->set_table('user');
                $crud->set_subject('<span style="font-size:27.9px">Si eres nuevo entra por aquí</span>');            
                //Fields

                //unsets
                $crud->unset_back_to_list()
                     ->unset_delete()
                     ->unset_read()
                     ->unset_edit()
                     ->unset_list()
                     ->unset_export()
                     ->unset_print()
                     ->field_type('fecha_registro','invisible')
                     ->field_type('fecha_actualizacion','invisible')
                     ->field_type('status','invisible')
                     ->field_type('foto','invisible')
                     ->field_type('admin','invisible')
                     ->field_type('password','password')
                     ->field_type('cedula','invisible')
                     ->field_type('apellido_materno','invisible')
                     ->field_type('tiene_android','hidden',1);
                if(!empty($_SESSION['pedido'])){
                    $crud->set_lang_string('insert_success_message','Sus datos han sido guardados con éxito <script>setTimeout(function(){document.location.href="'.base_url('pedidos/admin/procesar_pendiente').'"; },2000)</script>');
                }
                $crud->display_as('password','Contraseña nuevo usuario')                 
                     ->display_as('email','Email de contacto')                 ;
                $crud->set_lang_string('form_add','');
                $crud->required_fields('password','email','nombre','apellido');
                //Displays             
                $crud->set_lang_string('form_save','Registrarse');

                $crud->callback_before_insert(array($this,'binsertion'));
                $crud->callback_after_insert(array($this,'ainsertion'));
                $crud->set_rules('email','Email','required|valid_email|is_unique[user.email]');
                $output = $crud->render();
                $output->view = 'registro';
                $output->crud = 'user';
                $output->title = 'Registrarse';
                $output->output = get_header_crud($output->css_files,$output->js_files,TRUE).$output->output;
                $this->loadView($output);   
            }
        }              
        
        function conectar()
        {
            $this->loadView('predesign/login');
        }
        /* Callbacks */
        function binsertion($post)
        {            
            $post['status'] = 1;
            $post['admin'] = 0;
            $post['fecha_registro'] = date("Y-m-d H:i:s");
            $post['password'] = md5($post['password']);
            return $post;
        }
        
        function ainsertion($post,$primary)
        {              
            //Asignar rol
            get_instance()->db->insert('user_group',array('user'=>$primary,'grupo'=>3));            
            get_instance()->user->login_short($primary);
            get_instance()->db->insert('clientes',array('user_id'=>$primary));
            return true;
        }
        
       
        function forget($key = '',$ajax = '')
        {
            if(empty($_POST) && empty($key)){
                $this->loadView(array('view'=>'forget'));
            }
            else
            {
                if(empty($key)){
                if(empty($_SESSION['key'])){
                    $this->form_validation->set_rules('email','Email','required|valid_email');
                    if($this->form_validation->run())
                    {
                        $user = $this->db->get_where('user',array('email'=>$this->input->post('email')));
                        if($user->num_rows()>0){
                            $_SESSION['key'] = md5(rand(0,2048));
                            $_SESSION['email'] = $this->input->post('email');
                            if($_SESSION['lang']=='es'){
                                correo($this->input->post('email'),'reestablecimiento de contraseña',$this->load->view('email/forget',array('user'=>$user->row()),TRUE));
                            }else{
                                correo($this->input->post('email'),'Forgotten Password',$this->load->view('email/forget_en',array('user'=>$user->row()),TRUE));
                            }
                            if(empty($ajax)){
                                $_SESSION['msj'] = $this->success('Los pasos para la restauracion han sido enviados a su correo electronico');
                                header("Location:".base_url('registro/forget'));
                            }else{
                                echo $this->traduccion->traducir($this->success('Los pasos para la restauracion han sido enviados a su correo electronico'),$_SESSION['lang']);
                            }
                        }
                        else{
                            if(empty($ajax)){
                                $this->loadView(array('view'=>'forget','msj'=>$this->error('El correo que desea restablecer no esta registrado.')));
                            }else{
                                echo $this->traduccion->traducir($this->error('El correo que desea restablecer no esta registrado.'),$_SESSION['lang']);
                            }
                        }
                    }
                    else{
                        if(empty($ajax)){
                            $this->loadView(array('view'=>'forget','msj'=>$this->error($this->form_validation->error_string())));
                        }else{
                            $this->error($this->form_validation->error_string());
                        }
                    }
                }
                else
                {
                    $this->form_validation->set_rules('email','Email','required|valid_email');
                    $this->form_validation->set_rules('pass','Password','required|min_length[8]');
                    $this->form_validation->set_rules('pass2','Password2','required|min_length[8]|matches[pass]');
                    //$this->form_validation->set_rules('key','Llave','required');
                    if($this->form_validation->run())
                    {
                        /*if($this->input->post('key') == $_SESSION['key'])
                        {*/
                            $this->db->update('user',array('password'=>md5($this->input->post('pass'))),array('email'=>$_SESSION['email']));
                            session_unset();
                            $this->loadView(array('view'=>'forget','msj'=>$this->success('Se ha restablecido su contraseña <a href="'.base_url().'">Volver al inicio</a>')));
                        /*}
                        else
                            $this->loadView(array('view'=>'recover','msj'=>$this->error('Se ha vencido el plazo para el restablecimiento, solicitelo nuevamente.')));*/
                    }
                    else{
                        if(empty($_POST['key'])){
                            if(empty($ajax)){
                                $this->loadView(array('view'=>'forget','msj'=>$this->error('Se ha vencido el plazo para el restablecimiento, solicitelo nuevamente.')));    
                            }else{
                                echo $this->traduccion->traducir($this->error('Se ha vencido el plazo para el restablecimiento, solicitelo nuevamente.'),$_SESSION['lang']);
                            }
                            session_unset();
                        }
                        else{
                            $this->loadView(array('view'=>'recover','key'=>$key,'msj'=>$this->error($this->form_validation->error_string())));
                        }
                    }
                }
                }
                else
                {
                    if(!empty($_SESSION['key']) && $key==$_SESSION['key'])
                    {
                        $this->loadView(array('view'=>'recover','key'=>$key));
                    }
                    else{
                        if(empty($ajax)){
                            $this->loadView(array('view'=>'forget','msj'=>$this->error('Se ha vencido el plazo para el restablecimiento, solicitelo nuevamente.')));
                        }else{
                            echo $this->traduccion->traducir($this->error('Se ha vencido el plazo para el restablecimiento, solicitelo nuevamente.'),$_SESSION['lang']);
                        }
                    }
                }
            }
        }        
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
