<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
ob_start();
session_start();

class Main extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->helper('h');
        $this->load->database();
        $this->load->model('user');
        $this->load->model('querys');
        $this->load->library('grocery_crud');
        $this->load->library('ajax_grocery_crud');
        date_default_timezone_set('America/Asuncion');
    }

    public function index() {
        $log = $this->user;
        if ($log->log)
            redirect (base_url('panel'));
        else
            $this->loadView('main');
    }

    public function success($msj) {
        return '<div class="alert alert-success">' . $msj . '</div>';
    }

    public function error($msj) {
        return '<div class="alert alert-danger">' . $msj . '</div>';
    }

    public function login() {
        if (!$this->user->log) {
            if (!empty($_POST['email']) && !empty($_POST['pass'])) {
                $this->db->where('email', $this->input->post('email'));
                $r = $this->db->get('user');
                if ($this->user->login($this->input->post('email', TRUE), $this->input->post('pass', TRUE))) {
                    if ($r->num_rows() > 0 && $r->row()->status == 1) {
                        if (!empty($_POST['remember']))
                            $_SESSION['remember'] = 1;
                        if (empty($_POST['redirect']))
                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="' . site_url() . '"</script>');
                        else
                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="' . base_url($_POST['redirect']) . '"</script>');
                    }
                    else $_SESSION['msj'] = $this->error('El usuario se encuentra bloqueado, comuniquese con un administrador para solucionar su problema');
                } else
                    $_SESSION['msj'] = $this->error('Usuario o contrasena incorrecta, intente de nuevo.');
            } else
                $_SESSION['msj'] = $this->error('Debe completar todos los campos antes de continuar');

            if (!empty($_SESSION['msj']))
                header("Location:" . base_url());
        }
        header("Location:" . base_url('panel'));
    }

    public function unlog() {
        $this->user->unlog();
        header("Location:" . site_url());
    }

    function getHead($page){
        $ajustes = $this->db->get('ajustes')->row();
        //$stocookie = '<link href="'.base_url('js/stocookie/stoCookie.css').'">';        
        $stocookie = '';
        $page = str_replace('</head>',$stocookie.'</head>',$page);
        return $page;
    }

    function getBody($page){
        $ajustes = $this->db->get('ajustes')->row();        
        $stocookie = '<script src="'.base_url('js/stocookie/stoCookie.min.js').'"></script>';
        $stocookie.= html_entity_decode($ajustes->cookies);
        $page = str_replace('</body>',$stocookie.'</body>',$page);
        $page= str_replace('</body>',$ajustes->analytics.'</body>',$page);
        return $page;
    }

    public function loadView($param = array('view' => 'main')) {
        if (is_string($param))
            $param = array('view' => $param);
        $ajustes = $this->db->get('ajustes')->row();
        if(is_object($param)){
            $param = (array)$param;
        }
        if(empty($param['title'])){
            $param['title'] = $ajustes->titulo;
        }
        $param['favicon'] = $ajustes->favicon;
        $param['keywords'] = $ajustes->keywords;
        $param['description'] = $ajustes->description;
        $page = $this->load->view('template', $param,true);
        $page = $this->getHead($page);
        $page = $this->getBody($page);
        echo $page;
    }

    public function loadViewAjax($view, $data = null) {
        $view = $this->valid_rules($view);
        $this->load->view($view, $data);
    }
    
    function app()
    {            
        $this->loadView(array('view'=>'app'));
    }
    
    function error404(){
        echo 'Ha ocurrido un error';
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
