<?php
require_once APPPATH.'/controllers/Panel.php';    

class Vacunas extends Panel {

    function __construct() {
        parent::__construct();             
    }    
    function vacunacion(){
        $this->as['vacunacion'] = 'vacunas';
        $crud = $this->crud_function('','');        
        $crud = $crud->render();
        //$crud->set_clone();
        $this->loadView($crud);
    }
    function vacunas_dosis($x){
        $crud = $this->crud_function('','');     
        $crud->field_type('vacunas_id','hidden',$x)
             ->where('vacunas_id',$x)
             ->unset_columns('vacunas_id');                        
        $crud = $crud->render();
        $crud->header = new ajax_grocery_crud();
        $crud->header->set_table('vacunas')
                     ->set_theme('header_data')
                     ->set_subject('vacunas')
                     ->unset_add()
                     ->where('vacunas.id',$x)
                     ->unset_edit()
                     ->unset_delete()
                     ->unset_read()
                     ->unset_export()
                     ->unset_print()
                     ->set_url('vacunas/vacunacion/');
        $crud->header = $crud->header->render(1)->output;                     
        $crud->output = $this->load->view('vacunas_dependency',array('crud'=>$crud),TRUE);
        $crud->title = "Registro de dosis para vacunas";
        //$crud->set_clone();
        $this->loadView($crud);
    } 
    function vacunas_edades($x){
        $crud = $this->crud_function('',''); 
        $crud->field_type('vacunas_id','hidden',$x)
             ->where('vacunas_id',$x)
             ->unset_columns('vacunas_id');                        
        $crud = $crud->render();
        $crud->header = new ajax_grocery_crud();
        $crud->header->set_table('vacunas')
                     ->set_theme('header_data')
                     ->set_subject('vacunas')
                     ->unset_add()
                     ->where('vacunas.id',$x)
                     ->unset_edit()
                     ->unset_delete()
                     ->unset_read()
                     ->unset_export()
                     ->unset_print()
                     ->set_url('vacunas/vacunacion/');
        $crud->header = $crud->header->render(1)->output;                     
        $crud->output = $this->load->view('vacunas_dependency',array('crud'=>$crud),TRUE);
        $crud->title = "Registro de edades para vacunas";
        //$crud->set_clone();
        $this->loadView($crud);
    }   
}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
