<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Reportes extends Panel{
        function __construct() {
            parent::__construct();
            if(empty($this->user->facultad))
                header("Location:".base_url('panel/seleccionarFacultad'));
            $this->load->model('planacademico_model');
        }
        
        function verReportes(){
            $this->as['verReportes'] = 'reportes';
            $crud = $this->crud_function('','');
            $crud->where('activo',1);
            $crud->columns('nombre','Ver Reporte');
            $crud->unset_add()->unset_edit()->unset_delete()->unset_print()->unset_export()->unset_read();
            $crud->callback_column('Ver Reporte',function($val,$row){
                return '<a href="'.base_url($row->link).'">Ver Reporte</a>';
            });
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function filtrar(){
            $data = array();
            $this->load->model('querys');
            $this->db->select('sedes.id, sedes.sede_nombre as nombre');
            $data['sedes'] = $this->querys->getJSONData('sedes');
            $this->db->select('modalidades.id, modalidades.modalidad_nombre as nombre');
            $data['modalidad'] = $this->querys->getJSONData('modalidades');
            $this->db->select('carreras.id, carreras.carrera_nombre as nombre');
            $data['carreras'] = $this->querys->getJSONData('carreras');
            $this->db->select('programacion_carreras.id, programacion_carreras.programacion_nombre as nombre');
            if(!empty($_POST['modalidad'])){                
                $this->db->where('modalidades_id',$_POST['modalidad']);
            }
            if(!empty($_POST['sede'])){
                $this->db->where('sedes_id',$_POST['sede']);
            }
            if(!empty($_POST['carrera'])){
                $this->db->where('carreras_id',$_POST['carrera']);
            }
            $data['programacion_carreras'] = $this->querys->getJSONData('programacion_carreras');
            $this->db->select('plan_estudio.id, plan_estudio.plan_nombre as nombre');
            if(!empty($_POST['programacion_carrera'])){
                $this->db->where('programacion_carreras_id',$_POST['programacion_carrera']);
            }
            $data['plan_estudio'] = $this->querys->getJSONData('plan_estudio');
            $this->db->select('cursos.id, cursos.curso_nombre as nombre');
            $data['cursos'] = $this->querys->getJSONData('cursos');
            $this->db->select('secciones.id, secciones.seccion_nombre as nombre');
            $data['secciones'] = $this->querys->getJSONData('secciones');
            
            $this->db->select('programacion_materias_plan.id, materias.materia_nombre as nombre');
            $this->db->join('materias_plan','materias_plan.id = programacion_materias_plan.materias_plan_id','inner');
            $this->db->join('materias','materias.id = materias_plan.materias_id','inner');
            $this->db->join('plan_estudio','plan_estudio.id = materias_plan.plan_estudio_id','inner');
            $this->db->group_by('materias.id, programacion_materias_plan.anho_lectivo');
            $this->db->where('plan_estudio.facultades_id',$this->user->facultad);
            if(!empty($_POST['anho_lectivo'])){
                $this->db->where('programacion_materias_plan.anho_lectivo',$_POST['anho_lectivo']);
            }
            if(!empty($_POST['plan_estudio'])){
                $this->db->where('materias_plan.plan_estudio_id',$_POST['plan_estudio']);
            }
            
            if(!empty($_POST['curso'])){
                $this->db->where('materias_plan.cursos_id',$_POST['curso']);
            }            
            $data['programacion_materias_plan'] = $this->querys->getJSONData('programacion_materias_plan');
        
            echo json_encode($data);
            
        }
    }
?>
