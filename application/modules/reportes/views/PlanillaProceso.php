<body class="no-skin">
        <?php $this->load->view('includes/header') ?>
    <div class="main-container" id="main-container">
        <?php $this->load->view('includes/sidebar') ?>
        <div class="main-content">
            <div class="main-content-inner">
                <?php $this->load->view('includes/breadcum') ?>
                <div class="page-content">
                    <div class="page-header">
                        <h1>
                            <?= empty($title) ? 'Escritorio' : $title ?>
                            <small>
                                <i class="ace-icon fa fa-angle-double-right"></i>
                                <a href="<?= base_url('panel/seleccionarFacultad') ?>" title="Seleccionar facultad"><?php if(!empty($this->user->facultad)) echo '- '.$this->user->facultadName; else echo 'Facultad no seleccionada' ?></a>
                            </small>
                        </h1>
                    </div><!-- /.page-header -->

                    <div class="row">
                        <div class="col-xs-12">            
                            
                            <?php $this->load->view('predesign/chosen'); ?>
                            <?= !empty($msj)?$msj:'' ?>
                            <form method="post" id="formFiltro"> 
                              <div class="form-group">
                                <label for="exampleInputEmail1">Año Lectivo</label>
                                <input type="number" name="anho_lectivo" class="form-control">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputEmail1">Sede</label>
                                <?= form_dropdown_from_query('sede','sedes','id','sede_nombre') ?>
                              </div>
                              <div class="form-group">
                                <label for="exampleInputEmail1">Modalidad</label>
                                <?= form_dropdown_from_query('modalidad','modalidades','id','modalidad_nombre') ?>
                              </div>
                              <div class="form-group">
                                <label for="exampleInputEmail1">Carrera</label>
                                <?php $this->db->where('facultades_id',$this->user->facultad) ?>
                                <?= form_dropdown_from_query('carrera','carreras','id','carrera_nombre') ?>
                              </div>
                              <div class="form-group">
                                <label for="exampleInputEmail1">Programación Carrera</label>
                                <?php $this->db->where('facultades_id',$this->user->facultad) ?>
                                <?= form_dropdown_from_query('programacion_carrera','programacion_carreras','id','programacion_nombre') ?>
                              </div>
                              <div class="form-group">
                                <label for="exampleInputEmail1">Plan de estudio</label>
                                <?php $this->db->where('facultades_id',$this->user->facultad) ?>
                                <?= form_dropdown_from_query('plan_estudio','plan_estudio','id','plan_nombre') ?>
                              </div>
                              <div class="form-group">
                                <label for="exampleInputEmail1">Curso</label>
                                <?= form_dropdown_from_query('curso','cursos','id','curso_nombre') ?>
                              </div>
                              <div class="form-group">
                                <label for="exampleInputEmail1">Sección</label>
                                <?= form_dropdown_from_query('seccion','secciones','id','seccion_nombre') ?>
                              </div>
                              <div class="form-group">
                                <label for="exampleInputEmail1">Programación de materia</label>
                                <?php         
                                    $this->db->select('programacion_materias_plan.id, programacion_materias_plan.anho_lectivo, materias.materia_nombre');
                                    $this->db->join('materias_plan','materias_plan.id = programacion_materias_plan.materias_plan_id','inner');
                                    $this->db->join('materias','materias.id = materias_plan.materias_id','inner');
                                    $this->db->join('plan_estudio','plan_estudio.id = materias_plan.plan_estudio_id','inner');
                                    $this->db->group_by('materias.id, programacion_materias_plan.anho_lectivo');
                                    $this->db->where('plan_estudio.facultades_id',$this->user->facultad);
                                    echo form_dropdown_from_query('programacion_materias_plan_id','programacion_materias_plan','id','materia_nombre anho_lectivo');
                                ?>
                              </div>  
                              <button type="submit" class="btn btn-default">Ver Reporte</button>
                            </form>
                            <script>
                                $("select, input").change(function(){
                                    var data = new FormData(document.getElementById('formFiltro'));
                                    $.ajax({
                                        url:'<?= base_url('reportes/filtrar') ?>',
                                        context: document.body,
                                        data:data,
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        type: 'POST',
                                        success:function(data){
                                            data = JSON.parse(data);
                                            console.log(data);
                                            reeplaceSelectOption('select[name="sede"]',data.sedes);
                                            reeplaceSelectOption('select[name="modalidad"]',data.modalidad);
                                            reeplaceSelectOption('select[name="carrera"]',data.carreras);
                                            reeplaceSelectOption('select[name="programacion_carrera"]',data.programacion_carreras);
                                            reeplaceSelectOption('select[name="plan_estudio"]',data.plan_estudio);
                                            reeplaceSelectOption('select[name="curso"]',data.cursos);
                                            reeplaceSelectOption('select[name="seccion"]',data.secciones);
                                            reeplaceSelectOption('select[name="programacion_materias_plan_id"]',data.programacion_materias_plan);

                                        }
                                    });
                                });

                                function reeplaceSelectOption(label,data){
                                    var $el = $(label);
                                    var newOptions = data;
                                    var old = $(label).val();
                                    $el.empty(); // remove old options
                                    $el.append($('<option></option>').attr('value', '').text(''));        
                                    for(var i in newOptions){
                                        var key = newOptions[i].id;
                                        var value = newOptions[i].nombre;
                                        $el.append($('<option></option>')
                                        .attr('value', key).text(value.replace(/&(nbsp|amp|quot|lt|gt);/g,' ')));
                                        if(key==old){
                                            $el.val(old);
                                        }            
                                    }        
                                    $el.chosen().trigger('liszt:updated');
                                    $(".chzn-container").css('width','100%');
                                }
                            </script>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div>
        </div><!-- /.main-content -->			
    </div><!-- /.main-container -->
    <script src="<?= base_url("js/ace.min.js") ?>"></script>
    <script src="<?= base_url("js/jquery-ui.custom.min.js") ?>"></script>	
    <script src="<?= base_url("js/ace-elements.min.js") ?>"></script>
</body>