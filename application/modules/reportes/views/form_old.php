<?php $this->load->view('predesign/datepicker'); ?>
<?php $this->load->view('predesign/chosen'); ?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h1 class="panel-title"><?= $reporte->titulo ?></h1>
    </div>
    <div class="panel-body">
        <form method="post">
            <?php foreach($var as $v): ?>
                <?php $vari = explode(':',$v); ?>
                <?php if(count($vari)==1 || !isset($_POST[$vari[0]])): ?>
                    <?php if($v=='desde' || $v=='hasta'): ?>
                        <div class="form-group">
                          <label for="text"><?= str_replace('_',' ',ucwords($v)) ?></label>
                          <input id='<?= $v ?>' type="text" class="datetime-input form-control" name="<?= $v ?>" id="text" placeholder="<?= ucfirst($v) ?>">
                        </div>      
                    <?php elseif(!strstr($v,'_id')): ?>
                        <div class="form-group">
                          <label for="text"><?= str_replace('_',' ',ucwords($v)) ?></label>
                          <input type="text" class="form-control" name="<?= $v ?>" id="text" placeholder="<?= str_replace('_',' ',ucwords($v)) ?>">
                        </div>      
                    <?php elseif(strpos($v,'_id')): ?>
                        <div class="form-group">
                          <?php $table = str_replace('_id','',explode(':',$v)[0]); ?>
                          <?php $target = explode(':',$v);  ?>
                          <label for="text"><?= str_replace('_',' ',ucwords($target[0])) ?></label>
                          <?php $this->db->order_by($target[1]) ?>
                          <?= form_dropdown_from_query($target[0],$table,'id',$target[1]) ?>
                        </div>      
                    <?php endif ?>
                <?php endif ?>
            <?php endforeach ?>
            <div class="form-group">
                <input type='radio' name='docType' value='pdf'> PDF
                <input type='radio' name='docType' value='excel'> EXCEL
                <input type='radio' name='docType' value='html' checked=""> HTML
            </div>
            <button type="submit" class="btn btn-default">Consultar</button>
        </form>
    </div>
</div>
