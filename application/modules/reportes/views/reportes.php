<?php if(count($rep)==0): ?>
<div class="alert alert-info">
    Su lista de reportes esta vacia, puede organizar sus reportes en esta ventana desde el <a href="<?= base_url('reportes/rep/report_organizer') ?>">organizador de reportes</a>
</div>
<?php endif ?>
<div class="row">
    <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="tabbable">
            <ul class="nav nav-tabs padding-18 tab-size-bigger" id="myTab">
                <?php $usados = array(); ?>
                <?php foreach($rep as $n=>$r): ?>
                    <?php if(!in_array($r->nombre,$usados)): $usados[] = $r->nombre ?>
                        <li class="<?= $n==0?'active':'' ?>">
                            <a data-toggle="tab" href="#faq-tab-<?= $r->id ?>">
                                <i class="blue ace-icon <?= !empty($r->icono)?$r->icono:'fa fa-question-circle' ?> bigger-120"></i>
                                <?= $r->nombre ?>
                            </a>
                        </li>
                    <?php endif ?>
                <?php endforeach ?>
            </ul>

            <div class="tab-content no-border padding-24">
                <?php $usados = array(); ?>
                <?php foreach($rep as $n=>$r): ?>
                    <?php if(!in_array($r->nombre,$usados)): $usados[] = $r->nombre ?>
                        <div id="faq-tab-<?= $r->id ?>" class="tab-pane fade <?= $n==0?'in active':'' ?>">
                            <h4 class="blue">
                                <i class="ace-icon fa fa-check bigger-110"></i>
                                Listado de reportes <?= $r->nombre ?>
                            </h4>

                            <div class="space-8"></div>

                            <div id="faq-list-1" class="panel-group accordion-style1 accordion-style2">
                                <?php foreach(explode(',',$r->reportes) as $re): ?>
                                    <?php list($id,$nombre) = explode(':',$re); ?>
                                    <div class="list-group">
                                        <a href="<?= base_url('reportes/rep/verReportes/'.$id.'') ?>" class="list-group-item"><?= $nombre ?></a>
                                    </div>
                                <?php endforeach ?>

                            </div>
                        </div>
                    <?php endif ?>
                <?php endforeach ?>
                
            </div>
        </div>

        <!-- PAGE CONTENT ENDS -->
    </div><!-- /.col -->
</div>
