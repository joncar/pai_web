<link rel="stylesheet" href="<?= base_url('assets/grocery_crud/css/') ?>/jquery_plugins/bootstrap.datepicker/bootstrap.datepicker.css">
<script src="<?= base_url('assets/grocery_crud/js/') ?>/jquery_plugins/bootstrap.datepicker.js"></script>
<script src="<?= base_url('assets/grocery_crud/js/') ?>/jquery_plugins/config/jquery.datepicker.config.js"></script>
<?php $this->load->view('predesign/chosen'); ?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h1 class="panel-title"><?= $reporte->titulo ?></h1>
    </div>
    <div class="panel-body">
        <form method="post" id="formulario">
            <?php $this->load->view('_form'); ?>
        </form>
    </div>
</div>
<script>
    $(".date-input").datepicker({                    
        format: "yyyy-mm-dd",
        showButtonPanel: true,
        changeMonth: true,
        changeYear: true
    });
    var timeOut = '';
    var setAjaxSearch = function(){
        //Buscar parametro        
        var select = $(this).parents('.form-group').find('select');
        if(select.hasClass('ajax_query'))
        {            
            var valor = $(this).val();
            clearTimeout(timeOut);
            timeOut = setTimeout(function(){
                var data = new FormData(document.getElementById('formulario'));
                console.log(data);
                data.append('searchParam',valor);
                data.append('searchField',select.attr('name'));
                $.ajax({
                url: '<?= base_url('reportes/rep/mostrarForm/'.$reporte->id.'/1') ?>',
                data: data,
                context: document.body,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST'
             }).always(function(data){
                 $(document).off('focus','input,select');
                 $(document).off("change","#formulario > div > input,#formulario > div > select");
                 $("#formulario").html(data);
                 setTimeout(function(){refresh();},500);                 
                 $(".chosen-select").chosen({"search_contains": true, allow_single_deselect:true});
                 $(".chzn-search input").on("keyup",setAjaxSearch);
                 $(".date-input").datepicker({                    
                    format: "yyyy-mm-dd",
                    showButtonPanel: true,
                    changeMonth: true,
                    changeYear: true
                });
                //$(".ajax_query").trigger('change');
             });
            },500);                
        }
    }
    
    function refresh(){
        $(document).on('focus','input,select',function(){consult = true;});
        $(document).on("change","#formulario > div > input,#formulario > div > select",function(){
            if(consult){
                consult = false;
                var data = new FormData(document.getElementById('formulario'));
                $.ajax({
                    url: '<?= base_url('reportes/rep/mostrarForm/'.$reporte->id.'/1') ?>',
                    data: data,
                    context: document.body,
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST'
                 }).always(function(data){
                     $("#formulario").html(data);
                     $(".chosen-select").chosen({"search_contains": true, allow_single_deselect:true});
                     $(".chzn-search input").on("keyup",".chzn-search input",setAjaxSearch);
                     $(".date-input").datepicker({                
                        format: "yyyy-mm-dd",
                        showButtonPanel: true,
                        changeMonth: true,
                        changeYear: true
                    });
                 });
            }
        });
    }
    var consult = true;
    refresh();
    $(document).on("keyup",".chzn-search input",setAjaxSearch);
    
    
</script>