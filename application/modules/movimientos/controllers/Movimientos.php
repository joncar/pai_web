<?php

require_once APPPATH.'/controllers/Panel.php';    

class Movimientos extends Panel {

    function __construct() {
        parent::__construct();             
    }    
    function registrar_vacunas(){
    	$this->as['registrar_vacunas'] = 'registro_vacunacion';
        $crud = $this->crud_function('','');  
        $crud->set_subject('Registro de vacionación');
        $crud->set_lang_string('insert_success_message','Registro almacenado con éxito <script>setTimeout(function(){document.location.href="'.base_url().'movimientos/registro_vacunacion_detalle/{id}/add"},2000)</script>');      
        $crud->field_type('created','hidden',date("Y-m-d"))
        	 ->field_type('modified','hidden',date("Y-m-d"))
        	 ->field_type('anulado','hidden',0);
       	$crud->display_as('procesos_vacunas_id','Proceso de vacuna')
       		 ->display_as('zona_id','Zona de vacunación')
       		 ->display_as('region_id','Región de vacunación')
       		 ->display_as('distrito_id','Distrito de vacunación')
       		 ->display_as('local_servicio_id','Local donde se aplico el servicio')
       		 ->display_as('edades_id','Para las edades')
       		 ->display_as('region_vacunado_id','Region de vacunado')
       		 ->display_as('distrito_vacunado_id','Distrito de vacunado');
	 	$crud->columns('procesos_vacunas_id','fecha_inicio','fecha_fin','zona_id','region_id','distrito_id','local_servicio_id','edades_id');
        $crud->set_relation('distrito_vacunado_id','distrito','distrito_nombre');
        $crud->set_relation('region_vacunado_id','region','nombre');
       	$crud->set_relation_dependency('distrito_id','region_id','region_id');
       	$crud->set_relation_dependency('local_servicio_id','distrito_id','distrito_id');
        $crud = $crud->render();
        $crud->title = 'Registro de vacunación';
        //$crud->set_clone();
        $crud->output = $this->load->view('vacuna',array('output'=>$crud->output),TRUE);
        $this->loadView($crud);
    }     

    function registro_vacunacion_detalle($x = ""){    	
    	if(is_numeric($x)){
	        $crud = $this->crud_function('','');     
          $this->edad = @$this->db->get_where('registro_vacunacion',array('id'=>$x))->row()->edades_id;
	        $crud->set_subject('Registro de detalle de vacunación');
	        $crud->where('registro_vacunacion_id',$x);   
	        $crud->field_type('registro_vacunacion_id','hidden',$x);   
	        $crud->unset_columns('registro_vacunacion_id');
	        $crud->display_as('vacunas_id','Vacuna')
	        	 ->display_as('zona_vacunacion_id','Zona de vacunación')
	        	 ->display_as('nivel_escolar_id','Nivel escolar')
	        	 ->display_as('sexo_id','Sexo')
	        	 ->display_as('dosis_id','Dosis');
          $crud->callback_field('vacunas_id',function($val){
            $db = get_instance()->db;
            $db->select('vacunas.*');
            $db->join('vacunas','vacunas.id = vacunas_edades.vacunas_id');
            $db->where('vacunas_edades.edades_id',$this->edad);
            return form_dropdown_from_query('vacunas_id','vacunas_edades','id','vacunas_nombre',$val,'id="field-vacunas_id"');
          });
	        $crud = $crud->render();
	        $crud->header = new ajax_grocery_crud();
	        $crud->header->set_table('registro_vacunacion');
	        $crud->header->set_theme('header_data');
	        $crud->header->where('registro_vacunacion.id',$x);
	        $crud->header->columns('procesos_vacunas_id','fecha_inicio','fecha_fin','zona_id','region_id','distrito_id','local_servicio_id','edades_id');
	        $crud->header->set_url('movimientos/registrar_vacunas/');
	        $crud->header->display_as('procesos_vacunas_id','Proceso de vacuna')
			       		 ->display_as('zona_id','Zona de vacunación')
			       		 ->display_as('region_id','Región de vacunación')
			       		 ->display_as('distrito_id','Distrito de vacunación')
			       		 ->display_as('local_servicio_id','Local donde se aplico el servicio')
			       		 ->display_as('edades_id','Para las edades')
			       		 ->display_as('region_vacunado_id','Region de vacunado')
			       		 ->display_as('distrito_vacunado_id','Distrito de vacunado');
	        $crud->header = $crud->header->render(1)->output;
	        $crud->output = $this->load->view('vacuna_detalle',array('crud'=>$crud),TRUE);	        
	        $crud->title = 'Registro de detalle de vacunació';
	        $this->loadView($crud);
    	}else{
    		redirect('vacunas/registrar_vacuna');
    	}
    } 
}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
