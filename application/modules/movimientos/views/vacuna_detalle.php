<?= $crud->header ?>
<?= $crud->output ?>
<script>
	var vacunas = <?php 
		$data = array();
		$this->db->select('dosis.*,vacunas.id as vacunas_id');
		$this->db->join('dosis','dosis.id = vacunas_dosis.dosis_id');
		$this->db->join('vacunas','vacunas.id = vacunas_dosis.vacunas_id');
		foreach($this->db->get_where('vacunas_dosis')->result() as $v){
			$data[] = $v;
		}
		echo json_encode($data);
	?>;
	$("#field-vacunas_id").on('change',function(){
		var opt = '';
		for(var i in vacunas){
			if(vacunas[i].vacunas_id==$(this).val()){
				opt+= '<option value="'+vacunas[i].id+'">'+vacunas[i].dosis_nombre+'</option>';
			}
		}
		$("#field-dosis_id").html(opt);
		$("#field-dosis_id").chosen().trigger('liszt:updated');
	});
</script>