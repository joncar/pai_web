<?php

require_once APPPATH.'/controllers/Panel.php';    

class Maestras extends Panel {

    function __construct() {
        parent::__construct();             
    }    

    function tipo_contador($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->add_action('Detalles','',base_url('maestras/contadores/').'/');
        $output = $crud->render();            
        $this->loadView($output);
    }

    function contadores($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->where('tipo_contador_id',$x)
             ->field_type('tipo_contador_id',$x)
             ->unset_columns('tipo_contador_id');
        $output = $crud->render();        
        $this->loadView($output);
    }
    function region(){
        $crud = $this->crud_function('','');        
        $crud = $crud->render();
        //$crud->set_clone();
        $this->loadView($crud);
    } 
    function distrito(){
        $crud = $this->crud_function('','');        
        $crud = $crud->render();
        //$crud->set_clone();
        $this->loadView($crud);
    } 
    function dosis(){
        $crud = $this->crud_function('','');        
        $crud = $crud->render();
        //$crud->set_clone();
        $this->loadView($crud);
    } 
    function edades(){
        $crud = $this->crud_function('','');        
        $crud = $crud->render();
        //$crud->set_clone();
        $this->loadView($crud);
    } 
    function local_servicio(){
        $crud = $this->crud_function('','');        
        $crud = $crud->render();
        //$crud->set_clone();
        $this->loadView($crud);
    } 
    function nivel_escolar(){
        $crud = $this->crud_function('','');        
        $crud = $crud->render();
        //$crud->set_clone();
        $this->loadView($crud);
    } 
    function procesos_vacunas(){
        $crud = $this->crud_function('','');        
        $crud = $crud->render();
        //$crud->set_clone();
        $this->loadView($crud);
    } 
    function sexo(){
        $crud = $this->crud_function('','');        
        $crud = $crud->render();
        //$crud->set_clone();
        $this->loadView($crud);
    } 
    function zona(){
        $crud = $this->crud_function('','');        
        $crud = $crud->render();
        //$crud->set_clone();
        $this->loadView($crud);
    } 
    function zona_vacunacion(){
        $crud = $this->crud_function('','');        
        $crud = $crud->render();
        //$crud->set_clone();
        $this->loadView($crud);
    } 
    
}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
