<?php
class Gcm_library {
    function __construct()
    {
        $this->apiKey = 'AIzaSyBiTQp5QUWTeD0XQpNnv3kevaKrtokkpnA';
    }
    
    function Send($registrationIdsArray,$messageData)
    {
        $headers = array("Content-Type:" . "application/json", "Authorization:" . "key=" . $this->apiKey);
        $data = array(
        'data' => $messageData,
        'registration_ids' => $registrationIdsArray
        );

        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch, CURLOPT_URL, "https://android.googleapis.com/gcm/send" );
        curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, 0 );
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode($data) );
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }
    
    function encodeMsj($titulo,$msj)
    {
        // Message to send
        $message = $msj;
        $tickerText = rand(0,255);
        $contentTitle = $titulo;
        $contentText = $titulo;
        return array('message' => $message, 'tickerText' => $tickerText, 'contentTitle' => $contentTitle, "contentText" => $contentText,'title'=>$titulo);
    }
}
?>