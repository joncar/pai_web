<?php     
    $tables = json_decode(file_get_contents("http://siaweb.unp.edu.py/main/query"));    
?>
<html>
    <head>
            <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
            <link href="css/bootstrap-lightbox.min.css" rel="stylesheet" type="text/css" />
            <link href="css/style.css" rel="stylesheet" type="text/css" />
            <link href="css/dropzone.css" type="text/css" rel="stylesheet" />
            <!--[if lt IE 8]><style>
            .img-container span {
                display: inline-block;
                height: 100%;
            }
            </style><![endif]-->
            <script type="text/javascript" src="js/jquery.1.9.1.min.js"></script>
            <script type="text/javascript" src="js/bootstrap.min.js"></script>
            <script type="text/javascript" src="js/bootstrap-lightbox.min.js"></script>
            <script type="text/javascript" src="js/dropzone.min.js"></script>
            <script type="text/javascript" src="js/jquery.touchSwipe.min.js"></script>
            <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
            <link href="css/style.css" rel="stylesheet" type="text/css" />
            <script>
            $(function() {
              $( ".tablecell" ).draggable();
            } );
            
            </script>
    </head>
    <body>
        <div>
            <p><input type="text" name="Nombre" value="" placeholder="Nombre de variable" id="varname"></p>
            <!-- Nav tabs -->
            <div id='message'></div>
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Tablas</a></li>
              <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Transformador <span id='aprofile' class="badge badge-info">0</span></a></li>
              <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Consulta</a></li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="home">
                  <p class="alert alert-info">Selecciona las tablas que deseas consultar</p>
                  <?php foreach($tables as $t): ?>
                        <div class="draggable" data-table='<?= $t->Tables_in_unpedupy_academico ?>'>
                            <p><b><?= $t->Tables_in_unpedupy_academico ?></b></p>                            
                        </div>
                    <?php endforeach ?>
              </div>
                <div role="tabpanel" class="tab-pane" id="profile">
                    Lista vacia
                </div>
                <div role="tabpanel" class="tab-pane" id="messages">
                    <div class="consulta">
                        Consulta SQL
                        <div id='miconsulta'></div>
                        <a href='javascript:apply();' class="btn btn-success">Aplicar</a>
                    </div>
                </div>
            </div>
        </div>
        <script>
            
            function explode(search,string,limit){
                var q = [];
                string = string.split(search);
                q.push(string.shift());
                q.push(string.join(' = '));
                return q;
            }
            //If Edit Inspect the code
            var structure = <?php echo json_encode($tables) ?>;
            var tablas_seleccionadas = [];
            var query = '';
            var selects = [];
            var from = '';
            var joins = {};
            var wheres = [];
            
            <?php if(!empty($_GET['edit'])): ?>
                $(document).on('ready',function(){
                    var frame = window.parent.document.getElementById('<?= $_GET['editor'] ?>_ifr');
                    var content = $(frame).contents().find('#queryEdit').text();
                    var q = explode(' = ',content,2);
                    $("#varname").val(q[0]);
                    q = q[1];
                    //Si es select
                    if(q.indexOf('|selec|')>-1 && q.indexOf('from')){                        
                        var sel = q.split(' ');
                        var tables = [];
                        //Seleccionamos las tablas
                        for(var i in sel){
                            if(sel[i].indexOf('.')>-1){
                                var table = sel[i].split('.');
                                table = table[0];
                                if(tables.indexOf(table)===-1){                                
                                    $(".draggable[data-table='"+table+"']").trigger('click');
                                    tables.push(table);
                                }
                            }
                        }
                        //Hacemos el FROM
                        var from = q.split(' FROM ');
                        from = from[1];
                        from = explode(' ',from,2);
                        from = from[0];
                        $("."+from+" input[type='radio']").trigger('click');                        
                        
                        //Seleccionamos los campos
                        var from = q.split(' FROM ');
                        from = from[0];
                        from = from.replace('|selec| ','');
                        from = from.replace(/, /g,' ');
                        from = from.split(' ');
                        for(var i in from){
                            if(from[i].indexOf('.')>-1){
                                var field = from[i];
                                field = field.replace('.','_');
                                $(".sel_"+field).trigger('click');
                            }
                        }
                        
                        //JOINS
                        var from = q.split(' FROM ');
                        from = from[1];
                        from = from.split(' WHERE ');
                        from = from[0]; //Sin From sin Where
                        from = from.split(' ');
                        var ident = 1;                        
                        for(var i in from){
                            if(from[i]==='INNER' || from[i]==='LEFT' || from[i]==='RIGHT'){
                                i = parseInt(i);
                                var field1 = from[i+4];
                                var field2 = from[i+6];
                                $(".joincheck[data-table='"+field1+"']").val(ident+':1:'+from[i]);
                                $(".joincheck[data-table='"+field2+"']").val(ident+':2:'+from[i]);
                                $(".joincheck").trigger('change');
                                ident++;                                
                            }
                        }
                        console.log(from);
                        $("#miconsulta").html(q);
                    }
                    
                });
            <?php endif ?>
            
            $(".draggable").on('click',function(){
                if($(this).hasClass('active')){
                    $(this).removeClass('active');
                    var index = tablas_seleccionadas.indexOf($(this).data('table'));
                    if(index>-1){
                        tablas_seleccionadas.splice(index,1);
                    }
                }else{
                    tablas_seleccionadas.push($(this).data('table'));
                    $(this).addClass('active');
                }
                $("#aprofile").html(tablas_seleccionadas.length);
                drawTables();
                query = '';
                selects = [];
                from = '';
                joins = {};
                wheres = [];
                createQuery();
            });
            
            
            $(document).on('click','.selcheck',function(){
                if(!$(this).prop('checked')){
                    var index = selects.indexOf($(this).val());
                    if(index>-1){
                        selects.splice(index,1);
                    }
                }else{
                    selects.push($(this).val());
                }
                createQuery();
            });
            
            $(document).on('click','.frominput input',function(){
                from = $(this).val();
                createQuery();
            });
            
            $(document).on('change','.where',function(){
                wheres = [];
                var total = $(".where").length;
                var x = 0;
                $(".where").each(function(){ 
                    if($(this).val()!==''){
                        var val = $(this).val();
                        val = val.split(':');
                        if(val.length==1){
                            val[1] = 'AND';
                        }
                        wheres.push({table:$(this).data('table'),filtro:val[0],cond:val[1]});
                        console.log(wheres);
                    }
                    if(x==total){
                        createQuery();
                    }
                });
            });
            
            $(document).on('change','.joincheck',function(){
                joins = {};
                var total = $(".joincheck").length;
                var x = 0;
                $(".joincheck").each(function(){                    
                    var val = $(this).val();
                    if(val!==''){
                        val = val.split(':');
                        if(val.length>=2){
                            id = val[0];
                            pos = val[1];
                            type = typeof(val[2])!=='undefined'?val[2]:'INNER';
                            tab = $(this).data('table');
                            if(typeof(joins[id])==='undefined'){
                                joins[id] = [];
                            }
                            joins[id].push({pos:pos,tab:tab,type:type});
                        }
                    }
                    x++;                    
                    if(x==total){
                        createQuery();
                    }
                });
            });
            
            function createQuery(){
                if(selects.length>0){
                    query = '|selec| ';
                    var sel = '';                    
                    for(var i in selects){
                        sel+= ', <br/>'+selects[i];
                    }
                    sel = sel.substring(7);
                    query+= sel+' <br/>FROM '+from;
                    var join = '';
                    for(var i in joins){
                        if(joins[i].length>=2){
                            var x1 = parseInt(joins[i][0])<parseInt(joins[i][1])?joins[i][0]:joins[i][1];
                            var x2 = parseInt(joins[i][0])<parseInt(joins[i][1])?joins[i][1]:joins[i][0];
                            table = x2.tab.split('.');
                            table2 = x1.tab.split('.');
                            query+= "<br/> "+x2.type+" JOIN "+table[1]+" ON "+table[0]+"."+table[1]+" = "+table2[0]+'.'+table2[1];
                        }
                    }
                    if(wheres.length>0){
                        query+= "<br/> WHERE ";
                        for(var i in wheres){
                            var x = wheres[i];
                            var cond = i>0?' '+wheres[i].cond:'';
                            query+= "<br/> "+cond+" "+wheres[i].table+" = "+wheres[i].filtro;
                        }
                    }
                }
                
                $("#miconsulta").html(query);
            }
            
            function drawTables(){
                var str = '';
                for(var i in tablas_seleccionadas){
                    var table = structure.find(x=>x.Tables_in_unpedupy_academico===tablas_seleccionadas[i]);
                    var checked = '';
                    if(i===0){
                        checked = 'checked="true"';
                        from = table.Tables_in_unpedupy_academico;
                    }
                    str+= '<div class="tablecell '+table.Tables_in_unpedupy_academico+'"><p><b>'+table.Tables_in_unpedupy_academico+'</b> <span class="frominput">FROM <input type="radio" name="from" '+checked+' value="'+table.Tables_in_unpedupy_academico+'"></span></p>';
                    str+= '<table class="table">';
                    str+= '<tr><th>Sel</th><th>Field</th><th>Where</th><th>Join</th></tr>';
                    for(var k in table.fields){
                        str+= '<tr><td><input type="checkbox" class="selcheck sel_'+table.Tables_in_unpedupy_academico+'_'+table.fields[k].name+'" value="'+table.Tables_in_unpedupy_academico+'.'+table.fields[k].name+'"></td><td>'+table.fields[k].name+'</td><td><input type="text" placeholder="Val" class="where" data-table="'+table.Tables_in_unpedupy_academico+'.'+table.fields[k].name+'"></td><td><input type="text" class="joincheck" value="" data-table="'+table.Tables_in_unpedupy_academico+'.'+table.fields[k].name+'"></td></tr>';
                    }
                    str+= '</table>';
                    str+= '</div>';
                }
                $("#profile").html(str);
                $( ".tablecell" ).draggable();
            }
            
            function apply(){
                var win = window.parent;
                var track = '<?= $_GET['editor'] ?>';
                var target = win.document.getElementById(track+'_ifr');                
                var q = query!==''?'<div class="query">'+$("#varname").val()+' = '+query+';</div><p>&nbsp;</p>':'';
                <?php if(empty($_GET['edit'])): ?>
                $(target).contents().find('#tinymce').append(q);
                <?php else: ?>
                $(target).contents().find('#queryEdit').html(q);
                <?php endif ?>
                var closed = win.document.getElementsByClassName('mce-query');
                $(closed).find('.mce-close').trigger('click');
                console.log(win);
            }
        </script>
        
    </body>
</html>
