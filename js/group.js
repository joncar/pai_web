function Group(){
    this.list = [],
            this.add = function (cliente) {
                var pos = this.getPosition(cliente.id);
                if (pos === null)
                    this.list.push(cliente);
                else
                    this.list[pos] = cliente;
            };
    this.remove = function (cliente) {
        if (cliente !== undefined) {
            var aux = [];
            for (var i in this.list) {
                if (this.list[i].id !== cliente.id) {
                    aux.push(this.list[i]);
                }
            }
            this.list = aux;
        }
    };
    this.update = function (cliente) {
        for (var i in this.list) {
            if (this.list[i].id === cliente.id) {
                this.list[i] = cliente;
            }
        }
    };
    this.getFromId = function (id) {
        var cliente = null;
        for (var i in this.list) {
            if (this.list[i].id === id) {
                cliente = this.list[i];
            }
        }
        return cliente;
    };
    this.getPosition = function (id) {
        var cliente = null;
        for (var i in this.list) {
            if (this.list[i].id === id) {
                cliente = i;
            }
        }
        return cliente;
    };
}

/// Objetos 
Group.prototype.agregar = function(cliente){
    this.add(cliente);
    this.refreshIDS();
};

Group.prototype.borrar = function(id){
    var element = this.getFromId(id);
    if(element!==null){
        element.erase();
        this.remove(element);
        //Refrescar ids
        this.refreshIDS();
    }
};

Group.prototype.buscarVacio = function(){
    for(var i=0;i<this.list.length;i++){
        if(this.list[i].mark===undefined){
            return this.list[i];
        }
    }
    return null;
};

Group.prototype.refreshIDS = function(){
    for(var i=0;i<this.list.length;i++){
        this.list[i].id=i;
        this.list[i].refreshID();
    }
};

//Objeto Direccion
function Direccion(){
    this.id = -1;
    this.mark = undefined;
    this.dom = undefined;
    this.lat = undefined;
    this.lng = undefined;
    this.direccion = undefined;

    this.draw = function(){
        var espejo = $(document).find(".itinerarioinputs")[1];
        var newRow = $('<div class="itinerarioinputs panel panel-primary" data-id="'+this.id+'">'+$(espejo).html()+'</div>');
        newRow.find('.servNumber').addClass('servNumberDel');
        $(".itinerarioinputs").parent().append(newRow);
        newRow.find('.servNumber').html('<span><i class="fa fa-remove"></i></span>'+(this.id+1));
        newRow.find('.servDel').show();
        this.dom = newRow;
    };
    this.erase = function(){
        $(".itinerarioinputs[data-id="+this.id+"]").remove();
        if(this.mark!==undefined){
            this.mark.setMap(null);
        }
    };

    this.refreshID = function(){
        this.dom.attr('data-id',this.id);
        var obj = this;
        this.dom.find('input').each(function(){
            $(this).attr('name',$(this).data('name')+'['+obj.id+']'+'['+$(this).data('cell')+']');
        });
    };

    this.setMark = function(lat,lng){
        this.lat = lat;
        this.lng = lng;
        if(this.dom!==undefined){
            if(this.mark===undefined){
                this.mark = new google.maps.Marker({
                    position: new google.maps.LatLng(lat,lng),
                    map: map,
                    title:"Destino #"+this.id,
                    draggable:true,
                    icon:this.icon!==undefined?this.icon:base_url+'/img/clientemark.png'
                });
                var obj = this;
                google.maps.event.addDomListener(this.mark, 'dragend', function(e) {
                    if(obj.puedemarcar(e.latLng.lat(),e.latLng.lng())){
                        obj.lat = e.latLng.lat();
                        obj.lng = e.latLng.lng();
                        obj.dom.find('.lat').val(obj.lat);
                        obj.dom.find('.lon').val(obj.lng);
                        obj.dom.find('.numeracion').parent().show();
                        sumar();
                    }else{
                        obj.mark.setPosition(new google.maps.LatLng(obj.lat,obj.lng));
                    }
                });

            }else{
                this.mark.setPosition(new google.maps.LatLng(lat,lng));
            }
            map.setZoom($("#field-ciudades_id option:selected").data('zoom'));            
            this.dom.find('.lat').val(lat);
            this.dom.find('.lon').val(lng);            
        }else{            
            if(this.mark===undefined){
                this.mark = new google.maps.Marker({
                    position: new google.maps.LatLng(lat,lng),
                    map: map,
                    title:this.title!==undefined?this.title:"Destino #",
                    icon:this.icon!==undefined?this.icon:base_url+'img/clientemark.png'
                });
            }else{
                this.mark.setPosition(new google.maps.LatLng(lat,lng));
            }
        }
        directionsDisplay.set('directions', null);
        directionsDisplay.setMap(map);
    };

    this.buscarDireccion = function(){
        if(this.lat!==undefined && this.lng!==undefined){
            var geocoder = new google.maps.Geocoder;
            var obj = this;
            geocoder.geocode({'latLng':{lat:this.lat,lng:this.lng}}, function(results, status) {
                //calle
                var loc = results.length>0?results[0]:'';
                obj.dom.find('.servLugar').val(results[0].formatted_address);
                obj.direccion = results[0].formatted_address;
            });
        }
    };

    this.fillDataFromText = function(){
        var calle = this.dom.find('.servLugar').val();
        var calle2 = this.dom.find('.servCalle').val();
        if(calle==='' && calle2!==''){
            calle = calle2;
        }else if(calle!=='' && calle2!==''){
            calle = calle+' & '+calle2;
        }            
        if(calle!==''){
            this.direccion = calle;
            var geocoder = new google.maps.Geocoder();
            var address = calle+' '+$("#field-ciudades_id option:selected").html();
            var obj = this;
            geocoder.geocode({'address': address}, function(results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    if(obj.puedemarcar(results[0].geometry.location.lat(),results[0].geometry.location.lng())){
                        map.panTo(results[0].geometry.location);
                        obj.lat = results[0].geometry.location.lat();
                        obj.lng = results[0].geometry.location.lng();
                        obj.setMark(obj.lat,obj.lng);
                        console.log(results);
                        sumar();
                    }
                }
            });
        }
    };

    this.puedemarcar = function(lat,lng){
        var latLng = new google.maps.LatLng(lat,lng);
        var result1 = google.maps.geometry.poly.containsLocation(latLng,limitacion1);
        var result2 = google.maps.geometry.poly.containsLocation(latLng,limitacion2);
        if(!result1 && !result2){
            alert('Por favor marque una región dentro de la ciudad de '+$("#field-ciudades_id option:selected").html());
            return false;
        }
        
        this.limite1 = result1;
        this.limite2 = result2;
        return true;
    };

    this.setOptions = function(obj){
        if(this.puedemarcar(obj.lat,obj.lon)){
            this.dom.find('.servLugar').val(obj.direccion1);
            this.dom.find('.servCalle').val(obj.direccion2);
            this.dom.find('.numeracion').val(obj.numeracion);
            this.lat = obj.lat;
            this.lng = obj.lon;
            this.setMark(obj.lat,obj.lon);
            map.panTo(new google.maps.LatLng(obj.lat,obj.lon));
            sumar();
        }

        $("button[data-dismiss='modal']").trigger('click');
    };

    this.confirmado = function(){
        console.log(this.dom.find('.confirmar').prop('checked'));
        return this.dom.find('.confirmar').prop('checked');
    };
        
    this.setActualPosition = function(){
        var options = {timeout: 30000, enableHighAccuracy: false, maximumAge: 750000};  
        var obj = this;        
        if (navigator.geolocation){
            navigator.geolocation.getCurrentPosition(function(position) {
               var c = position.coords;
               if(obj.puedemarcar(obj.lat,obj.lng)){
                   obj.lat = c.latitude;
                   obj.lng = c.longitude;
                   obj.setMark(obj.lat,obj.lng);
                   obj.buscarDireccion();
               }
            },function(e){
                console.log(e);
                alert('No se pudo obtener la posición actual');
            },options);
        } else { 
            alert("Geolocation no es soportada por este navegador.");
        }
    };
}

function ruta(){
    var directionsService = new google.maps.DirectionsService();    
    directionsDisplay.set('directions', null);
    directionsDisplay.setMap(map);
    var origen = grupo.list[0];
    var destino = grupo.list[grupo.list.length-1];
    var waypoints = [];
    for(var i in grupo.list){
        waypoints.push({location:new google.maps.LatLng(grupo.list[i].lat,grupo.list[i].lng),stopover:true});
    }
    directionsService.route({
        origin: new google.maps.LatLng(origen.lat,origen.lng),
        destination: new google.maps.LatLng(destino.lat,destino.lng),
        waypoints:waypoints,
        travelMode: google.maps.TravelMode.DRIVING
        }, function(response, status) {
        if (status === google.maps.DirectionsStatus.OK) {
          directionsDisplay.setDirections(response);          
        }
    });      
}